package com.biom4st3r.moenchantments.mixin.events;

import com.biom4st3r.moenchantments.api.events.LivingEntityDamageEvent;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;

@Mixin(LivingEntity.class)
public abstract class LivingEntity_damage_Mxn {
    @Inject(
        at = @At("HEAD"), 
        method = "damage", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    public void LivingEntityEvent(DamageSource damageSource_1, float float_1, CallbackInfoReturnable<Boolean> ci)
    {
        LivingEntityDamageEvent.EVENT.invoker().onDamage(damageSource_1, float_1, (Entity)(Object)this, ci);
    }
}