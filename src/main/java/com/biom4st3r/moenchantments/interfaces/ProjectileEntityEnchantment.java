package com.biom4st3r.moenchantments.interfaces;

import net.minecraft.enchantment.Enchantment;

/**
 * ProjectileEntityEnchantment
 */
public interface ProjectileEntityEnchantment {

    public void setEnchantmentLevel(Enchantment e, int level);
    public int getEnchantmentLevel(Enchantment e);

}