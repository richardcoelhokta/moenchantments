package com.biom4st3r.moenchantments.networking;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.ProjectileEntityEnchantment;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

/**
 * Packets
 */
public final class Packets {
    private static Identifier ADDITIONAL_ARROW_DATA = new Identifier(ModInit.MODID,"additarrowdata");

    public static final class SERVER
    {
        public static Packet<?> createAdditionalArrowData(ProjectileEntityEnchantment e)
        {
            PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
            int lvl = e.getEnchantmentLevel(EnchantmentRegistry.GRAPNEL);
            if(lvl < 1) return null;
            buf.writeInt(((Entity)e).getEntityId());
            buf.writeByte(lvl);

            return ServerSidePacketRegistry.INSTANCE.toPacket(ADDITIONAL_ARROW_DATA,buf);
        }
        public static void init(){// load class
        }
    }
    
    // @Environment(EnvType.CLIENT)
    public static final class CLIENT
    {
        static
        {
            ClientSidePacketRegistry.INSTANCE.register(ADDITIONAL_ARROW_DATA, (ctx,buf)->
            {
                int entityId = buf.readInt();
                int lvl = buf.readByte();
                ctx.getTaskQueue().execute(()->
                {
                    ProjectileEntityEnchantment e = (ProjectileEntityEnchantment) ctx.getPlayer().getEntityWorld()
                            .getEntityById(entityId);
                    if(e != null) e.setEnchantmentLevel(EnchantmentRegistry.GRAPNEL, lvl);

                });
            });
        }
        public static void init(){// load class
        }
    }
    
    public static void init(){// load class
        SERVER.init();
        if(FabricLoader.getInstance().getEnvironmentType() != EnvType.SERVER) CLIENT.init();
    }
    
}