package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.moenchantments.ModInit;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.server.MinecraftServer;

@Mixin(MinecraftServer.class)
public abstract class PostInitServer
{
    @Inject(at = @At("RETURN"),method="startServer")
    private static void postInit(CallbackInfoReturnable<MinecraftServer> ci)
    {
        ModInit.logger.log("...and my turn again");
        ModInit.whitelistToBlock();
        ModInit.blacklistAutosmelt();
    }
}