package com.biom4st3r.moenchantments;

import java.util.function.Function;

import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.MoEnchantBuilder;
import com.google.common.collect.Sets;

import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.FishingRodItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * EnchantmentRegistry
 * Centeral store for Enchantment instances and registry
 */
public class EnchantmentRegistry {
    public static final String MODID = ModInit.MODID;
    
    private static Function<ItemStack,Boolean> isBowOrCrossBow = (stack)->
    {
        for(Class<?> clazz : Sets.<Class<?>>newHashSet(FishingRodItem.class,BowItem.class,CrossbowItem.class))
        {
            if(clazz.isInstance(stack.getItem())) return true;
        }
        return false;
    };

    public static final EnchantmentSkeleton TREEFELLER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(ModInit.config.TreeFellerMaxBreakByLvl.length)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof AxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(ModInit.config.EnableTreeFeller)
        .build("treefeller");
    public static final EnchantmentSkeleton VEINMINER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(ModInit.config.VeinMinerMaxBreakByLvl.length)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof PickaxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(ModInit.config.EnableVeinMiner)
        .build("veinminer");
    public static final EnchantmentSkeleton AUTOSMELT = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.SILK_TOUCH)
        .enabled(ModInit.config.EnableAutoSmelt)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof MiningToolItem;})
        .addExclusive(Enchantments.SILK_TOUCH)
        .minpower((level)->{return level * 8;})
        .build("autosmelt");
    public static final EnchantmentSkeleton TAMEDPROTECTION = new MoEnchantBuilder(Rarity.COMMON, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof SwordItem || itemStack.getItem() instanceof AxeItem;})
        .treasure(true)
        .enabled(ModInit.config.EnableTamedProtection)
        .build("tamedprotection");
    public static final EnchantmentSkeleton ENDERPROTECTION = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.ARMOR, MoEnchantBuilder.ARMOR_SLOTS)
        .maxlevel(3)
        .minpower((level)->{return level*10;})
        .treasure(true)
        .curse(true)
        .enabled(ModInit.config.EnableEnderProtection)
        .build("curseofender");
    public static final EnchantmentSkeleton HAVESTER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .minpower((level)->{return level*4;})
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof HoeItem;})
        .enabled(false)
        .build("harvester");
    //Gitlab @Mr Cloud
    public static final EnchantmentSkeleton SOULBOUND = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.ALL_SLOTS)
        .enabled(ModInit.config.EnableSoulBound)
        .maxlevel(ModInit.config.UseStandardSoulboundMechanics ? 1 : ModInit.config.MaxSoulBoundLevel)
        .minpower((level)->{return 20+level;})
        .isAcceptible((itemstack)->{return true;})
        .build("soulbound");
    public static final EnchantmentSkeleton POTIONRETENTION = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .minpower((level)->{return 7*level;})
        .maxlevel(ModInit.config.PotionRetentionMaxLevel)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof SwordItem || itemstack.getItem() instanceof AxeItem;})
        .enabled(ModInit.config.EnablePotionRetention)
        .build("potionretension");
    // someone responding to Jeb_ on reddit.
    public static final EnchantmentSkeleton BOW_ACCURACY = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .maxlevel(2)
        .minpower((level)->{return (level-1)*10;})
        .maxpower((level)->{return (level-1)*10+5;})
        .enabled(ModInit.config.EnableAccuracy && !ModInit.extraBowsFound)
        .isAcceptible(isBowOrCrossBow)
        .build("bowaccuracy");
    public static final EnchantmentSkeleton BOW_ACCURACY_CURSE = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .maxlevel(1)
        .curse(true)
        .addExclusive(BOW_ACCURACY)
        .isAcceptible(isBowOrCrossBow)
        .enabled(ModInit.config.EnableInAccuracy && !ModInit.extraBowsFound)
        .build("bowinaccuracy");
    // Gitlab @cryum
    public static final EnchantmentSkeleton ARROW_CHAOS = new MoEnchantBuilder(Rarity.VERY_RARE,EnchantmentTarget.BOW,MoEnchantBuilder.HAND_SLOTS)
        .curse(true)
        .isAcceptible(isBowOrCrossBow)
        .minpower((level)->{return 30;})
        .enabled(ModInit.config.EnableArrowChaos && !ModInit.extraBowsFound)
        .build("arrow_chaos");
    // cryum from issue #13
    public static final EnchantmentSkeleton GRAPNEL = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .minpower(level->20*level)
        .maxlevel(2)
        .treasure(true)
        .enabled(ModInit.config.EnableGrapnel)
        .isAcceptible((stack)->
        {
            // SnowballItem
            // EnderPearlItem
            // EnderEyeItem
            return isBowOrCrossBow.apply(stack) || stack.getItem() instanceof FishingRodItem ;
        })
        .addExclusive(Enchantments.MULTISHOT)
        .build("grapnel");


    public static void init()
    {
        for(EnchantmentSkeleton e : new EnchantmentSkeleton[] {
            TREEFELLER,VEINMINER,AUTOSMELT,
            TAMEDPROTECTION,ENDERPROTECTION,POTIONRETENTION,
            HAVESTER,SOULBOUND,BOW_ACCURACY,
            BOW_ACCURACY_CURSE,ARROW_CHAOS,GRAPNEL,
            })
        {
            if(e.enabled())
                Registry.register(Registry.ENCHANTMENT, new Identifier(MODID, e.regName()), e);
        }
    }
}